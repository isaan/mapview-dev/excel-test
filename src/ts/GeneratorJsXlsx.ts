import Xlsx from 'xlsx';

export class Generator {
    private workbook: Xlsx.WorkBook = {
        SheetNames: [],
        Sheets: {},
    };

    public load(file: File): void {
        console.log('Creating the generator...');

        const reader: FileReader = new FileReader();

        reader.onload = (e: any) => {
            const data = new Uint8Array(e.target.result);
            this.workbook = Xlsx.read(data, {type: 'array'});
            console.log('Workbook imported.');
        };

        reader.onloadend = (e: any) => {
            console.log(this.workbook);
            this.copySheet('TableHeavydynModel', 'TableHeavydyn');
            this.removeSheet('TableHeavydynModel');
        };

        reader.readAsArrayBuffer(file);
    }

    private copySheet(from: string, to: string): void {
        const oldSheet: Xlsx.WorkSheet = this.workbook.Sheets[from];
        const newSheet: Xlsx.WorkSheet = JSON.parse(JSON.stringify(oldSheet));
        Xlsx.utils.book_append_sheet(this.workbook, newSheet, to);
    }

    private removeSheet(name: string): void {
        // Impossible proprement ?
        delete this.workbook.Sheets[name];
        delete this.workbook.SheetNames[this.workbook.SheetNames.indexOf(name)];
        // Xlsx.utils.
    }
}
