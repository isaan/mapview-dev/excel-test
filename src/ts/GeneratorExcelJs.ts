import * as Excel from 'exceljs';

export class Generator {
    private workbook = new Excel.Workbook();

    public load(file: File): void {
        const reader: FileReader = new FileReader();

        reader.onload = (event: any) => {
            // new Excel.Workbook().xlsx
            //     .load((event.target as any).result)
            //     .then((workbook) => {
            //         console.log(workbook);

            //         // this.workbook = workbook;
            //         // console.log(this.workbook);
            //         // this.copySheet('TableHeavydynModel', 'TableHeavydyn');
            //         // this.removeSheet('TableHeavydynModel');
            //     });

            // const data = new Uint8Array(event.target.result);
            // this.workbook = new Excel.Workbook().xlsx.read(data);
        };

        reader.readAsArrayBuffer(file);
    }

    private copySheet(from: string, to: string): void {
        const oldSheet: Excel.Worksheet = this.workbook.getWorksheet(from);
        const newSheet: Excel.Worksheet = this.workbook.addWorksheet('Sheet');

        newSheet.model = oldSheet.model;
        newSheet.name = to;
    }

    private removeSheet(name: string): void {
        this.workbook.removeWorksheet(name);
    }
}
